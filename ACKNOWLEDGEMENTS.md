Acknowledgements
----------------

We are very grateful to all the people who provided inputs, suggestions, support and bug reports, including:

- Florian Mueller (Institut Pasteur, Paris)
- Davide Mazza (Ospedale San Raffaele, Milano)
- Kevin Tsui (UC Berkeley)
- Sheila Teves (UC Berkeley)
- Amy Strom (UC Berkeley)
- Laura Caccianini (Curie Institute, Paris)
- Thomas Etheridge (University of Sussex)
- Charles Kervrann (INRIA Rennes)
